'use strict';
const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });

const security = require('./security');

const someFunction = require('./functions/someFunction');

function setLogData(req) {
	return {
		time: Date.now(),
		headers: JSON.stringify(req.headers),
		ip: JSON.stringify(req.ip),
		browser: req.headers['user-agent'],
		language: req.headers['accept-language']
	};
}

exports.someFunction = functions.https.onRequest((req, res) => {
	cors(req, res, function() {
		res.header('Content-Type', 'application/json');
		for (let i = 0; i < security.length; i++) {
			res.setHeader(security[i].name, security[i].value);
		}

		someFunction.handler(req, res);
	});
});
