const security = [
	{
		name: 'Cache-Control',
		value: 'max-age=31536000, immutable, public'
	},
	{
		name: 'Strict-Transport-Security',
		value: 'max-age=31536000; includeSubDomains;'
	},
	{
		name: 'X-Content-Type-Options',
		value: 'nosniff'
	},
	{
		name: 'X-Frame-Options',
		value: 'DENY'
	},
	{
		name: 'X-Powered-By',
		value: ''
	},
	{
		name: 'X-XSS-Protection',
		value: '1; mode=block'
	},
	{
		name: 'Content-Security-Policy',
		value:
			"default-src 'none'; script-src 'self' https://cdnjs.cloudflare.com connect-src 'self'; img-src 'self' data:; style-src 'self' https://cdnjs.cloudflare.com; font-src 'self' https://cdnjs.cloudflare.com; manifest-src 'self'; frame-ancestors 'none'; form-action 'none'; block-all-mixed-content"
	}
];

module.exports = security;
